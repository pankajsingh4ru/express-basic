const express = require("express");
const bodyParser = require("body-parser");
const customers = require('./customers.json');

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.status(201).json({ message: "done!" });
});

app.get("/customers/list",(req,res) =>{  
  try {
    var reqData = req.query.filter
    var customerData = customers;
    var cust = [];
   for (const customer of customerData) {
    
      if(customer.first_name.toLowerCase().includes(reqData.toLowerCase()) || customer.last_name.toLowerCase().includes(reqData.toLowerCase())){
          cust.push(customer)
      }
   }
    res.status(201).json({message:"data fetch successfully", data:cust});
  } catch (error) {
    res.status(200).json({message:"Not found"});
  }
})

app.get("/customers",(req,res)=>{
  try {
   
    const {car_make,first_name,last_name,email,gender} = req.query
    let results = [...customers];
    if (first_name) {
      results = results.filter(r => r.first_name.toUpperCase() === first_name.toUpperCase());
    }

    if (car_make) {
        results = results.filter(r => r.car_make.toUpperCase() === car_make.toUpperCase() || r.car_make.toLocaleLowerCase() === car_make.toLocaleLowerCase());
    }
    if (email) {
        results = results.filter(r => r.email.toUpperCase() === email.toUpperCase() || r.email.toLocaleLowerCase() === email.toLocaleLowerCase());
    }

    if (gender) {
      results = results.filter(r => r.gender.toUpperCase() === gender.toUpperCase() || r.gender.toLocaleLowerCase() === gender.toLocaleLowerCase());
    }
    

    if (last_name) {
      results = results.filter(r => r.last_name.toUpperCase() === last_name.toUpperCase() || r.last_name.toLocaleLowerCase() === last_name.toLocaleLowerCase());
    }
   
    res.status(201).json({message:"data fetch successfully",results});
  } catch (error) {
    console.log(error)
    res.status(200).json({message:"Not found"});
  }
})

app.listen(8080, function () {
  console.log("Server is running on 8080");
});
